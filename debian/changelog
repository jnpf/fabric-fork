fabric (2.6.0-1) unstable; urgency=medium

  * Team upload.
  * Set Rules-Requires-Root: no
  * Use https for Homepage URL
  * Switch to debhelper-compat 13
  * Switch to dh-sequence-python3
  * Use d/clean instead of override
  * Update d/watch version
  * Override very-long-line-length-in-source-file warning
  * Set Section: python for python3 module package
  * Add d/upstream/metadata file
  * De-duplicate short description in d/control
  * Bump Standards-Version to 4.6.0
  * Update d/copyright
  * Move maintenance to Python Team (Closes: #984559)
  * New upstream version 2.6.0

 -- Luca Boccassi <bluca@debian.org>  Sat, 19 Feb 2022 13:51:01 +0000

fabric (2.5.0-0.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Move dependency on python3-decorator from fabric to python3-fabric
    (Closes: #1002026)

 -- Luca Boccassi <bluca@debian.org>  Tue, 28 Dec 2021 13:37:52 +0000

fabric (2.5.0-0.3) unstable; urgency=medium

  * Non-maintainer upload.
  * Add dependency on python3-decorator (Closes: #956320)

 -- Luca Boccassi <bluca@debian.org>  Mon, 31 May 2021 11:00:56 +0100

fabric (2.5.0-0.2) unstable; urgency=medium

  * Non-maintainer upload.
  * No changes source-only upload to allow migration to testing.

 -- Luca Boccassi <bluca@debian.org>  Sun, 08 Dec 2019 15:58:47 +0000

fabric (2.5.0-0.1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ Luca Boccassi ]
  * Update upstream source from tag 'upstream/2.5.0' (Closes: #930451)
  * Refresh 0001-Remove-Google-AdSense-tracker-from-docs.patch to remove
    fuzz
  * Remove 0002-disable-failed-tests-with-fudge10.patch, no longer
    required
  * Convert to python3 and split executable in its own package (Closes:
    #916815, #936501)
  * Add Built-Using metadata generated by sphinx
  * Non-maintainer upload.

 -- Luca Boccassi <bluca@debian.org>  Thu, 28 Nov 2019 18:21:31 +0000

fabric (1.14.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.2, no changes.

 -- Andrew Starr-Bochicchio <asb@debian.org>  Mon, 25 Dec 2017 20:55:31 -0500

fabric (1.13.1-4) unstable; urgency=medium

  * debian/rules: Disable test_should_use_sentinel_for_tasks_that_errored
    The test randomly fails in certain environments causing more
    too much noise to be useful (Closes: #854686).

 -- Andrew Starr-Bochicchio <asb@debian.org>  Tue, 16 May 2017 22:20:39 -0400

fabric (1.13.1-3) unstable; urgency=medium

  * Run tests in verbose mode (Closes: #857094).

 -- Andrew Starr-Bochicchio <asb@debian.org>  Tue, 07 Mar 2017 20:10:33 -0500

fabric (1.13.1-2) unstable; urgency=medium

  * debian/rules: Disable test_nested_execution_with_explicit_ports
    when running tests at build time as it hangs indefinitely
    when run on a single core VM (Closes: #850230).

 -- Andrew Starr-Bochicchio <asb@debian.org>  Wed, 18 Jan 2017 22:32:02 -0500

fabric (1.13.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop 0003-allow-paramiko-2.patch, applied upstream.
  * Bump debian/compat to 10.

 -- Andrew Starr-Bochicchio <asb@debian.org>  Sat, 24 Dec 2016 13:35:22 -0500

fabric (1.12.0-1) unstable; urgency=medium

  * New upstream release.
  * 0002-disable-failed-tests-with-fudge10.patch: Disable tests that
    require Fudge < 1.0 so that we can run the rest of the test suite.
  * debian/rules: Re-enable the test suite.
  * debian/control:
   - Build depend on python-fudge and python-nose.
   - Bump Standards-Version to 3.9.8, no changes.
   - Build depend and depend on python-crypto.
   - Drop unneeded depend on python-nose.
  * 0003-allow-paramiko-2.patch: Patch setup.py to allow
    Paramiko 2.

 -- Andrew Starr-Bochicchio <asb@debian.org>  Sat, 12 Nov 2016 12:45:54 -0500

fabric (1.10.2-1) unstable; urgency=medium

  [ Stein Magnus Jodal ]
  * New upstream release.
  * debian/control:
    - Build-Depend on debhelper >= 9.
    - Build-Depend on python-all >= 2.7.
    - Build-Depend on dh-python.
    - Replace debian/pyversions with X-Python-Version declaration.
  * debian/copyrights:
    - Use DEP5 format.
    - Update upstream and package copyrights.
    - Update upstream license from GPLv2 to BSD-2-clause, as upstream changed
      license in 2009.
  * Install docs with dh_sphinxdoc. This fixes embedded-javascript-library
    Lintian warning for Underscore.js.
  * Update debian/watch.
  * Use pybuild as build system.
  * Replace AdSense removal patch with a new cleanly applying patch with the
    same license as the upstream source.

  [ Andrew Starr-Bochicchio ]
  * Add myself as Maintainer (Closes: #797593).
  * Fix typo in debian/watch.
  * debian/fab.1: Update to be more comprehensive based on
    on the upstream usage docs.

 -- Andrew Starr-Bochicchio <asb@debian.org>  Tue, 15 Sep 2015 22:32:39 -0400

fabric (1.10.0-1) unstable; urgency=medium

  * New upstream release. (Closes: #760152)
  * Update versioned depends on python-paramiko to >= 1.10.
    Thanks David Banks for the suggestion. (Closes: #740930)
  * Update Vcs-* fields.
  * Packaging updates to generate the documentation:
    - Add b-d on python-alabaster.
    - Drop patch disable_sphinx_extension.patch.
    - Update debian/rules accordingly.
    - Update no_adsense patch to work with the new doc system.
  * Update to Standards-Version 3.9.6, no changes required.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sun, 05 Oct 2014 02:35:21 +0200

fabric (1.8.2-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer. (Closes: #738292)
  * Bump debian/compat to 8
  * Update Standards-Version: 3.9.5, no changes required.
  * Change Vcs-Git and Vcs-Git fields to my github copy.
  * Modify debian/copyright to add myself in the packaging copyright.
  * Add patch no_adsense.patch, to remove Google Adsense in the html documentation.
  * Disable sphinx extension "releases", it is not included in Debian. See
    patch disable_sphinx_extension.patch.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Sat, 22 Feb 2014 23:01:27 +0100

fabric (1.7.0-2) unstable; urgency=medium

  * Orphaning package.

 -- Chris Lamb <lamby@debian.org>  Sat, 08 Feb 2014 23:50:33 +0000

fabric (1.7.0-1) unstable; urgency=low

  * New upstream release.
    Thanks to Rodney Lorrimar <rlorrimar@ccg.murdoch.edu.au>.
  * Drop 01-paramiko.diff - equivalent change made upstream.

 -- Chris Lamb <lamby@debian.org>  Tue, 06 Aug 2013 12:06:30 +0100

fabric (1.4.3-1) unstable; urgency=low

  * New upstream release (Closes: #680209)
  * Drop 02-no-upstream-tags-when-building.diff.
  * Bump Standards-Version to 3.9.3.

 -- Chris Lamb <lamby@debian.org>  Mon, 29 Oct 2012 23:59:13 +0000

fabric (1.4.2-1.1) unstable; urgency=low

  * Fix paramiko incompatiblity with python-ssh. Thanks to Emmanuel Bouthenot
    (Closes: #677479)

 -- Chris Lamb <lamby@debian.org>  Fri, 22 Jun 2012 10:23:10 +0100

fabric (1.4.2-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Wed, 13 Jun 2012 16:31:01 +0100

fabric (1.3.2-5) unstable; urgency=low

  * Move to dh_python2 from python-support. Patch by/via Micah Gersten
    <micahg@ubuntu.com>. (Closes: #653322)
  * Add missing python-paramiko to Build-Depends. (Closes: #653321)

 -- Chris Lamb <lamby@debian.org>  Tue, 03 Jan 2012 12:43:42 +0000

fabric (1.3.2-4) unstable; urgency=low

  * Call "install" in override_dh_auto_install, not "clean".
    (Closes: #651599)
  * Add missing python-sphinx dependency. (Closes: #651597, #651602)

 -- Chris Lamb <lamby@debian.org>  Wed, 14 Dec 2011 16:41:43 +0000

fabric (1.3.2-3) unstable; urgency=low

  * Ship documentation. (Closes: #649610)

 -- Chris Lamb <lamby@debian.org>  Thu, 08 Dec 2011 14:51:01 +0000

fabric (1.3.2-2) unstable; urgency=low

  * Use paramiko instead of "ssh" library. It is not yet packaged for Debian
    and paramiko appears to work in my quick tests and is better than it just
    failing. (Closes: #649161)
  * Add new python-nose dependency.

 -- Chris Lamb <lamby@debian.org>  Fri, 18 Nov 2011 15:17:45 +0000

fabric (1.3.2-1) unstable; urgency=low

  * New upstream release (Closes: #648483)
  * Update debian/watch.

 -- Chris Lamb <lamby@debian.org>  Wed, 16 Nov 2011 17:32:19 +0000

fabric (1.1.2-1) unstable; urgency=low

  * New upstream release.

 -- Chris Lamb <lamby@debian.org>  Thu, 28 Jul 2011 19:32:36 +0200

fabric (1.0.2-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.2.

 -- Chris Lamb <lamby@debian.org>  Tue, 28 Jun 2011 19:54:22 +0100

fabric (1.0.1-1) unstable; urgency=low

  * New upstream release (Closes: #627123)

 -- Chris Lamb <lamby@debian.org>  Tue, 17 May 2011 21:54:50 +0100

fabric (0.9.3-1) unstable; urgency=low

  * New upstream release (Closes: #612361)
  * Update Vcs-{Git,Browser}.

 -- Chris Lamb <lamby@debian.org>  Sun, 27 Feb 2011 21:27:34 +0000

fabric (0.9.2-1) unstable; urgency=low

  * New upstream release. (Closes: #597973)

 -- Chris Lamb <lamby@debian.org>  Thu, 07 Oct 2010 21:42:30 +0100

fabric (0.9.1-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.1.

 -- Chris Lamb <lamby@debian.org>  Sun, 01 Aug 2010 16:14:40 -0400

fabric (0.9.0-3) unstable; urgency=low

  * Don't try and install changes/ directory; something in sid now means this
    is causing a FTBFS. (Closes: #587138)
  * Switch to dpkg v3 source format.
  * Bump Standards-Version to 3.8.4.

 -- Chris Lamb <lamby@debian.org>  Mon, 28 Jun 2010 10:44:43 +0100

fabric (0.9.0-2) unstable; urgency=low

  * Don't embed python-paramiko. (Closes: #561398)

 -- Chris Lamb <lamby@debian.org>  Tue, 05 Jan 2010 21:47:56 +0000

fabric (0.9.0-1) unstable; urgency=low

  * New upstream release (Closes: #555796)
  * Update debian/watch.
  * debian/control:
    - Add missing binary dependency on python-pkg-resources (Closes: #557144)
    - Update Homepage field.
    - Bump Standards-Version.

 -- Chris Lamb <lamby@debian.org>  Mon, 23 Nov 2009 01:28:29 +0000

fabric (0.1.1-1) unstable; urgency=low

  * New upstream release.
  * Set section to 'net' from 'python'.
  * Bump Standards-Version to 3.8.1

 -- Chris Lamb <lamby@debian.org>  Sat, 09 May 2009 12:59:18 +0100

fabric (0.1.0-1) unstable; urgency=low

  * New upstream release.
  * Correct title of manual page.
  * Correct typo in debian/copyright.
  * Updating location of Git URLs.

 -- Chris Lamb <lamby@debian.org>  Sun, 22 Feb 2009 02:49:44 +0000

fabric (0.0.9-2) unstable; urgency=low

  * Add missing Build-Depends-Indep on python-setuptools. Found by checking
    source-only upload to Ubuntu.

 -- Chris Lamb <lamby@debian.org>  Thu, 08 Jan 2009 23:36:39 +0000

fabric (0.0.9-1) unstable; urgency=low

  * Initial release. (Closes: #504799)

 -- Chris Lamb <lamby@debian.org>  Sat, 08 Nov 2008 03:29:25 +0000
